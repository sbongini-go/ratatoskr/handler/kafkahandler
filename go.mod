module gitlab.com/sbongini-go/ratatoskr/handler/kafkahandler

go 1.20

require (
	github.com/confluentinc/confluent-kafka-go/v2 v2.0.2
	gitlab.alm.poste.it/go/ilog/ilog v0.1.2
	gitlab.com/sbongini-go/mapstructureplus v0.0.0-20211221083647-be241237238d
	gitlab.com/sbongini-go/ratatoskr/core v0.0.0-20211222090201-09d9e7320f83
	go.opentelemetry.io/otel v1.3.0
	go.opentelemetry.io/otel/trace v1.3.0
	gopkg.in/yaml.v2 v2.4.0
	gotest.tools v2.2.0+incompatible
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/go-logr/logr v1.2.1 // indirect
	github.com/go-logr/stdr v1.2.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/icza/dyno v0.0.0-20210726202311-f1bafe5d9996 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/client_golang v1.11.0 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.26.0 // indirect
	github.com/prometheus/procfs v0.6.0 // indirect
	gitlab.alm.poste.it/go/ilog/stdlog v0.1.2 // indirect
	go.opentelemetry.io/otel/exporters/jaeger v1.2.0 // indirect
	go.opentelemetry.io/otel/sdk v1.3.0 // indirect
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)
