package kafkahandler

import (
	"fmt"
	"testing"

	"gopkg.in/yaml.v2"
	"gotest.tools/assert"
)

func CommonTestUnmarshal(t *testing.T, in interface{}) {
	yamlData, err := yaml.Marshal(in)
	assert.NilError(t, err)
	fmt.Println("yaml:", string(yamlData))

	// UnMarshal
	cfg := make(map[string]interface{})
	err = yaml.Unmarshal(yamlData, &cfg)
	assert.NilError(t, err)

	out, err := NewFromInterface(cfg)
	assert.NilError(t, err)
	fmt.Println("out:", out)

	// Marshal
	yamlData2, err := yaml.Marshal(out)
	assert.NilError(t, err)
	fmt.Println("yaml2:", string(yamlData2))

	assert.Equal(t, string(yamlData), string(yamlData2))
}

func TestUnmarshal(t *testing.T) {
	// Marshal
	CommonTestUnmarshal(t, New(map[string]interface{}{
		"bootstrap.servers": "127.0.0.1:9092",
		"group.id":          "myGroup",
		"auto.offset.reset": "earliest",
	}, "topic-out",
		WithHeaderKey("key"),
	))
}
