package kafkahandler

import (
	"context"
	"fmt"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"gitlab.alm.poste.it/go/ilog/ilog"
	"gitlab.com/sbongini-go/mapstructureplus"
	"gitlab.com/sbongini-go/ratatoskr/core"
	"gitlab.com/sbongini-go/ratatoskr/core/handler"
	"gitlab.com/sbongini-go/ratatoskr/core/model"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"
)

func NewFromInterface(data interface{}) (core.Handler, error) {
	h := &kafkaHandler{}
	err := mapstructureplus.PrivateDecode(data, h)
	return h, err
}

func (h kafkaHandler) MarshalYAML() (interface{}, error) {
	return struct {
		Name      string
		Type      string
		Config    map[string]interface{}
		Topic     string
		HeaderKey string
	}{
		Name:      h.Name(),
		Type:      h.Type(),
		Config:    h.KafkaConfig,
		Topic:     h.Topic,
		HeaderKey: h.HeaderKey,
	}, nil
}

type kafkaHandler struct {
	// embedded type contente le parti comuni degli handlers
	handler.CommonHandler `mapstructure:",squash"`

	// Mappa contente le configurazioni per il producer Kafka
	KafkaConfig map[string]interface{} `yaml:"config" mapstructure:"config"`

	// Topic Kafka su cui produrre i messaggi
	Topic string `yaml:"topic" mapstructure:"topic"`

	// HeaderKey header dell'evento in input il cui valore va usato come key del messaggio inviato su Kafka
	HeaderKey string `yaml:"headerkey" mapstructure:"headerkey"`

	// producer Kafka
	producer *kafka.Producer `yaml:"-"`

	// cache
	cacheConnectionString string `yaml:"-"`
	cacheClientID         string `yaml:"-"`
}

// KafkaHandler istanzia un nuovo KafkaHandler
func New(kafkaConfig map[string]interface{}, topic string, opts ...Option) core.Handler {
	h := kafkaHandler{
		KafkaConfig: kafkaConfig, // Imposto le configurazioni per produrre su Kafka
		Topic:       topic,       // Imposto il topic su cui produrre i messaggi
	}

	// Applico gli option patterns
	for _, opt := range opts {
		opt(&h)
	}

	return &h
}

// Init funzione di inizializzazione del Handler
func (h *kafkaHandler) Init(ctx context.Context, initBag core.InitHandlerBag) error {
	h.SetLogger(initBag.Logger)

	if value, ok := h.KafkaConfig["bootstrap.servers"]; ok {
		h.cacheConnectionString = fmt.Sprintf("%v", value)
	}
	if value, ok := h.KafkaConfig["client.id"]; ok {
		h.cacheClientID = fmt.Sprintf("%v", value)
	}

	return nil
}

// Start avvia l'handler avviando il client Kafka per produrre messaggi
func (h *kafkaHandler) Start(ctx context.Context) error {
	// Preparo l'oggetto per configurare il producer
	producerConfig := kafka.ConfigMap{}
	for k, v := range h.KafkaConfig {
		producerConfig[k] = v
	}

	// Istanzio il producer Kafka
	producer, err := kafka.NewProducer(&producerConfig)
	if nil != err {
		return err
	}
	h.producer = producer

	return nil
}

// Execute esegue la logica del Handler
func (h kafkaHandler) Execute(ctx context.Context, bridge *core.Bridge, event model.Event) (model.Event, error) {
	span := trace.SpanFromContext(ctx) // Recupero lo span corrente
	span.SetAttributes(
		attribute.String("span.kind", trace.SpanKindProducer.String()),
		semconv.MessagingSystemKey.String("kafka"),
		semconv.MessagingDestinationKindTopic,
		semconv.MessagingDestinationKey.String(h.Topic),
		//semconv.MessagingTempDestinationKey.Bool(false),
		//semconv.MessagingProtocolKey.String("kafka"),
		//semconv.MessagingProtocolVersionKey.String("0.0.0"),
	)
	if h.cacheConnectionString != "" {
		span.SetAttributes(semconv.MessagingURLKey.String(h.cacheConnectionString))
	}
	if h.cacheClientID != "" {
		semconv.MessagingKafkaClientIDKey.String(h.cacheClientID)
	}

	// Creo il canale su cui verra' notificato l'esito del produce
	deliveryChan := make(chan kafka.Event)
	defer close(deliveryChan)

	// Utilizzando il campo HeaderKey recupero dall'header del evento
	// il valore da usare come key del messaggio Kafka
	var key *string = nil
	if h.HeaderKey != "" {
		if value, ok := event.Metadata[h.HeaderKey]; ok && len(value) > 0 {
			key = &value[0]
			// Rimuovo dai Metadati dell'evento l'header usato per la chiave
			// cosi da non propagarlo ulteriormente negli headers Kafka
			delete(event.Metadata, h.HeaderKey)
		}
	}

	// Preparo gli headers Kafka
	headers := make([]kafka.Header, len(event.Metadata))
	var i int = 0
	for key, value := range event.Metadata {
		headers[i] = kafka.Header{
			Key:   key,
			Value: []byte(value[0]), // XXX tengo solo il primo header
		}
		i++
	}

	// Creo il messaggio Kafka da inviare
	msg := &kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &h.Topic, Partition: kafka.PartitionAny},
		Value:          event.Data,
		Headers:        headers,
	}

	// Tramite L'HeaderKey ero riuscito a trovare una Key, la imposto nel messaggio
	if nil != key {
		msg.Key = []byte(*key)
		span.SetAttributes(semconv.MessagingKafkaMessageKeyKey.String(string(msg.Key)))
	}

	// Inietto lo span dentro l'header del Kafka message
	otel.GetTextMapPropagator().Inject(ctx, NewMessageCarrier(msg))

	// Produco il messaggio su Kafka
	err := h.producer.Produce(msg, deliveryChan)
	if nil != err {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return model.Event{}, err
	}

	// Attendo la produzione del messaggio
	e := <-deliveryChan
	m := e.(*kafka.Message)

	if nil != m.TopicPartition.Error {
		// La produzione e' andata in errore
		span.RecordError(m.TopicPartition.Error)
		span.SetStatus(codes.Error, m.TopicPartition.Error.Error())

		return model.Event{}, m.TopicPartition.Error
	}

	// Se sono giunto qua la produzione e' andata a buon fine
	if h.Logger().Enabled(ilog.DEBUG) {
		h.Logger().
			Val("topic", *m.TopicPartition.Topic).
			Val("partition", m.TopicPartition.Partition).
			Val("offset", m.TopicPartition.Offset).
			Debug("delivered message")
	}

	return model.Event{}, nil
}

// Stop arresta l'handler
func (h *kafkaHandler) Stop(ctx context.Context) error {
	if h.producer != nil {
		h.producer.Close()
	}
	return nil
}

// Type restituisce il tipo di handler
func (h *kafkaHandler) Type() string {
	return "kafka"
}
