package kafkahandler

// Option tipo funzione che rappresenta un opzione funzionale per inizializzare il kafkahandler
type Option func(*kafkaHandler)

// WithHeaderKey stringa che rappresenta l'HeaderKey ovvero la chiave delle header dell'evento in input
// il cui valore va usato come key del messaggio inviato su Kafka
func WithHeaderKey(headerKey string) Option {
	return func(c *kafkaHandler) {
		c.HeaderKey = headerKey
	}
}
